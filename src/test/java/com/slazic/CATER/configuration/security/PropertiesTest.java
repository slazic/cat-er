package com.slazic.CATER.configuration.security;

import com.slazic.CATER.configuration.email.EmailProperties;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("h2")
public class PropertiesTest {

    @Autowired
    private JwtProperties jwtProperties;

    @Autowired
    private EmailProperties emailProperties;

    @Test
    public void assertThatJwtPropertiesAreNotNull() {
        assertNotNull(jwtProperties.getDuration());
        assertNotNull(jwtProperties.getIssuer());
        assertNotNull(jwtProperties.getKey());

        assertTrue(jwtProperties.getDuration() > 0);
        assertTrue(jwtProperties.getIssuer().length() > 0);
        assertTrue(jwtProperties.getKey().length() > 0);
    }

    @Test
    public void assertThatEmailPropertiesAreNotNull() {
        assertNotNull(emailProperties.getSender());
        assertNotNull(emailProperties.getRedirectUrl());
        assertNotNull(emailProperties.getDuration());

        assertTrue(emailProperties.getSender().length() > 0);
        assertTrue(emailProperties.getRedirectUrl().length() > 0);
        assertTrue(emailProperties.getDuration() > 0);
    }
}
