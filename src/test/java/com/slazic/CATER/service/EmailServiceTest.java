package com.slazic.CATER.service;

import com.slazic.CATER.configuration.email.EmailProperties;
import com.slazic.CATER.model.User;
import com.slazic.CATER.repository.UserRepository;
import com.slazic.CATER.utility.StringGenerator;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.ActiveProfiles;

import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class EmailServiceTest {

    private EmailService emailService;

    @Mock
    private JavaMailSender mailSender;

    @Mock
    private EmailProperties emailProperties;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        emailService = new EmailService(mailSender, emailProperties, userRepository);
    }

    @Test
    public void sendVerificationEmail() throws MessagingException {
        final String recipient = StringGenerator.generateRandomEmail();
        final String hashedIdentifier = "hashedValueForTest";
        final String emailContent = "Email test content";

        when(emailProperties.getRedirectUrl()).thenReturn("https://example.com/verify");
        when(mailSender.createMimeMessage()).thenReturn(mock(MimeMessage.class));
        when(userRepository.save(any(User.class))).thenReturn(mock(User.class));
        doNothing().when(mailSender).send(any(MimeMessage.class));

        System.out.println("Email sender: " + emailProperties.getSender());

        emailService.sendVerificationEmail(recipient, hashedIdentifier);

        verify(mailSender, times(1)).send(any(MimeMessage.class));
    }

    @Test
    public void resendVerificationEmail() throws MessagingException {
        final User user = mock(User.class);
        final String recipient = StringGenerator.generateRandomEmail();
        final String hashedIdentifier = "hashedValueForTest";

        when(user.getEmail()).thenReturn(recipient);
        when(user.getHashedIdentifier()).thenReturn(hashedIdentifier);
        when(userRepository.save(any(User.class))).thenReturn(mock(User.class));
        doNothing().when(emailService).sendVerificationEmail(anyString(), anyString());

        emailService.resendVerificationEmail(user);

        verify(userRepository, times(1)).save(any(User.class));
        verify(emailService, times(1)).sendVerificationEmail(recipient, hashedIdentifier);
    }

}