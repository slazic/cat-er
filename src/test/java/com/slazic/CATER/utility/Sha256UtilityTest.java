package com.slazic.CATER.utility;

import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;

class Sha256UtilityTest {

    @Test
    void testGetHash() {
        String value = "password123@gmail.com";
        String hash = Sha256Utility.getHash(value);
        assertNotNull(hash);
        assertTrue(hash.length() > 0);
        assertTrue(hash.length() <= 64);
    }

    @Test
    void testGetHashWithEmptyString() {
        String value = "";
        String hash = Sha256Utility.getHash(value);
        assertNotNull(hash);
        assertTrue(hash.length() > 0);
        assertTrue(hash.length() <= 64);
    }

    @Test
    void testGetHashWithNull() {
        try {
            String value = null;
            String hash = Sha256Utility.getHash(value);
            fail("Expected NullPointerException to be thrown");
        } catch (final NullPointerException exception) {
            assertEquals("Cannot invoke \"String.getBytes(java.nio.charset.Charset)\" because \"value\" is null", exception.getMessage());
        }
    }
}