package com.slazic.CATER.utility;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmailValidatorTest {

    @Test
    public void testValidEmail() {
        String email = "test.email@example.com";
        assertTrue(EmailValidator.test(email));
    }

    @Test
    public void testEmailWithInvalidCharacters() {
        String email = "test.email@exa$ple.com";
        assertFalse(EmailValidator.test(email));
    }

    @Test
    public void testEmailWithMissingAtSymbol() {
        String email = "test.emailexample.com";
        assertFalse(EmailValidator.test(email));
    }

    @Test
    public void testEmailWithMissingDomain() {
        String email = "test.email@";
        assertFalse(EmailValidator.test(email));
    }

    @Test
    public void testEmailWithLeadingWhiteSpace() {
        String email = "   test.email@example.com";
        assertFalse(EmailValidator.test(email));
    }

    @Test
    public void testEmailWithTrailingWhiteSpace() {
        String email = "test.email@example.com   ";
        assertFalse(EmailValidator.test(email));
    }

    @Test
    public void testEmptyEmail() {
        String email = "";
        assertFalse(EmailValidator.test(email));
    }

    @Test
    public void testNullEmail() {
        try {
            String email = null;
            EmailValidator.test(email);
            fail("Expected NullPointerException to be thown");
        } catch (final NullPointerException exception) {
            assertEquals("Cannot invoke \"java.lang.CharSequence.length()\" because \"this.text\" is null", exception.getMessage());
        }
    }
}