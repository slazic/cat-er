package com.slazic.CATER.utility;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringGeneratorTest {

    @Test
    void testGenerateRandomString() {
        int length = 10;
        String randomString = StringGenerator.generateRandomString(length);
        assertNotNull(randomString);
        assertEquals(length, randomString.length());
    }

    @Test
    void testGenerateRandomEmail() {
        String randomEmail = StringGenerator.generateRandomEmail();
        assertNotNull(randomEmail);
        assertTrue(randomEmail.contains("@"));
    }
}