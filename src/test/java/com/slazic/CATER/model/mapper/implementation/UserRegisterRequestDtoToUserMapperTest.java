package com.slazic.CATER.model.mapper.implementation;

import com.slazic.CATER.model.User;
import com.slazic.CATER.model.enums.UserRole;
import com.slazic.CATER.model.request.UserRegisterRequestDto;
import com.slazic.CATER.utility.Sha256Utility;
import com.slazic.CATER.utility.StringGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

//@SpringBootTest commented out because not using @Autowire
//@ActiveProfiles("h2") no real components
@ExtendWith(MockitoExtension.class) //needed because using @Mock and @InjectMocks <- not working and not needed if using @MockBean
class UserRegisterRequestDtoToUserMapperTest {

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserRegisterRequestDtoToUserMapper mapper;

    /*
         @BeforeEach //needed if I want to use @MockBean on dependency
         public void setUp() {
         mapper = new UserRegisterRequestDtoToUserMapper(passwordEncoder)
            // Set up the mock behavior for the password encoder
            when(passwordEncoder.encode(any(CharSequence.class)))
                    .thenReturn("encoded_password");
        }
    */

    @Test
    void map() {
        // Given
        UserRegisterRequestDto requestDto = new UserRegisterRequestDto();
        requestDto.setFirstName(StringGenerator.generateRandomString(32));
        requestDto.setLastName(StringGenerator.generateRandomString(32));
        requestDto.setEmail(StringGenerator.generateRandomEmail());
        requestDto.setPassword(StringGenerator.generateRandomString(32));

        // When
        User user = mapper.map(requestDto);

        // Then
        assertEquals(requestDto.getFirstName(), user.getFirstName());
        assertEquals(requestDto.getLastName(), user.getLastName());
        assertEquals(requestDto.getEmail(), user.getEmail());
        assertEquals(passwordEncoder.encode(requestDto.getPassword()), user.getPassword());
        assertEquals(UserRole.USER, user.getUserRole());
        assertNotNull(user.getCreatedAt());
        assertEquals(false, user.getVerified());
        assertEquals(Sha256Utility.getHash(requestDto.getEmail()), user.getHashedIdentifier());
        assertEquals(64, user.getHashedIdentifier().length());
    }

    @Test
    void mapToList() {
        // Given
        UserRegisterRequestDto requestDto1 = new UserRegisterRequestDto();
        requestDto1.setFirstName(StringGenerator.generateRandomString(32));
        requestDto1.setLastName(StringGenerator.generateRandomString(32));
        requestDto1.setEmail(StringGenerator.generateRandomEmail());
        requestDto1.setPassword(StringGenerator.generateRandomString(32));

        UserRegisterRequestDto requestDto2 = new UserRegisterRequestDto();
        requestDto2.setFirstName(StringGenerator.generateRandomString(32));
        requestDto2.setLastName(StringGenerator.generateRandomString(32));
        requestDto2.setEmail(StringGenerator.generateRandomEmail());
        requestDto2.setPassword(StringGenerator.generateRandomString(32));

        // When
        List<User> users = mapper.mapToList(List.of(requestDto1, requestDto2));

        // Then
        assertEquals(2, users.size());

        User user1 = users.get(0);
        assertEquals(requestDto1.getFirstName(), user1.getFirstName());
        assertEquals(requestDto1.getLastName(), user1.getLastName());
        assertEquals(requestDto1.getEmail(), user1.getEmail());
        assertEquals(passwordEncoder.encode(requestDto1.getPassword()), user1.getPassword());
        assertEquals(UserRole.USER, user1.getUserRole());
        assertEquals(false, user1.getVerified());
        assertEquals(Sha256Utility.getHash(requestDto1.getEmail()), user1.getHashedIdentifier());
        assertEquals(64, user1.getHashedIdentifier().length());

        User user2 = users.get(1);
        assertEquals(requestDto2.getFirstName(), user2.getFirstName());
        assertEquals(requestDto2.getLastName(), user2.getLastName());
        assertEquals(requestDto2.getEmail(), user2.getEmail());
        assertEquals(passwordEncoder.encode(requestDto2.getPassword()), user2.getPassword());
        assertEquals(UserRole.USER, user2.getUserRole());
        assertNotNull(user2.getCreatedAt());
        assertEquals(false, user2.getVerified());
        assertEquals(Sha256Utility.getHash(requestDto1.getEmail()), user1.getHashedIdentifier());
        assertEquals(64, user1.getHashedIdentifier().length());

    }
}