package com.slazic.CATER.repository;

import com.slazic.CATER.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(final @NotNull String email);

    Optional<User> findByHashedIdentifier(final @NotNull String hash);
}
