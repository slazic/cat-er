package com.slazic.CATER.exception;

import javax.validation.constraints.NotNull;

public class EmailAlreadyInUseException extends Exception{
    public EmailAlreadyInUseException (final @NotNull String message){
        super(message);
    }
}
