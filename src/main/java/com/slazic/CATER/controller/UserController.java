package com.slazic.CATER.controller;

import com.slazic.CATER.model.request.UserChangePasswordDto;
import com.slazic.CATER.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.security.Principal;

@Slf4j
@RequestMapping("/user")
@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(
            final @NotNull UserService userService
    ) {
        this.userService = userService;
    }

    @GetMapping("/getAll")
    public ResponseEntity<Object> getAllUsers () {
        try{
            return ResponseEntity
                    .ok(userService.fetchAllUsers());
        } catch (final Exception exception) {
            log.error("Failed to fetch.", exception);
            return ResponseEntity
                    .status(500)
                    .body(exception);
        }
    }

    @GetMapping("/getUserProfile")
    public ResponseEntity<Object> getUserProfile(
            final Principal principal
            ) {
        try {
            return ResponseEntity
                    .ok(userService.loadUserByEmail(principal.getName()));
        } catch (final Exception exception) {
            log.error("Failed to fetch user profile.", exception);
            return ResponseEntity
                    .status(500)
                    .body(exception);
        }
    }

    @PutMapping("/changeUserAccess")
    public ResponseEntity<Object> changeUserAccess(@RequestParam final Long id, @RequestParam final Boolean access) {
        try {
            userService.changeUserAccess(id, access);
            return ResponseEntity
                    .ok("Access has been changed for selected user.");
        } catch (final Exception exception) {
            log.error("Failed to lock user.", exception);
            return ResponseEntity
                    .status(500)
                    .body(exception);
        }
    }

    @PostMapping("/changePassword")
    public ResponseEntity<Object> changeUserPasswrod(
            @RequestBody final UserChangePasswordDto userChangePasswordDto,
            final Principal principal
    ) {
        try {
            userService.changeUserPassword(userChangePasswordDto, principal.getName());
            return ResponseEntity
                    .ok("Password has been changed.");
        } catch (final Exception exception) {
            log.error("Failed to change password.", exception);
            return ResponseEntity
                    .status(500)
                    .body(exception);
        }
    }
}
