package com.slazic.CATER.controller;

import com.slazic.CATER.service.ExternalApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/fetchExternal")
@Slf4j
public class ExternalApiController {

    private final ExternalApiService apiService;

    @Autowired
    public ExternalApiController(
            final @NotNull ExternalApiService apiService
    ) {
        this.apiService = apiService;
    }

    @GetMapping("/binance5MinInterval")
    public ResponseEntity<Object> get5MinIntervalCandlestickDataFromBinance(
            @RequestParam final List<String> symbols
    ) {
        try{
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(apiService.fetch5MinIntervalCandlestickDataFromBinance(symbols));
        } catch (final Exception exception) {
            log.error("Failed to fetch.", exception);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(exception);
        }
    }

    @GetMapping("/singleCandlestickDataFromSelectedMarket")
    public ResponseEntity<Object> getSingleCandlestickDataFromSelectedMarket(
            @RequestParam final String symbol,
            @RequestParam final String timeframe,
            @RequestParam final String exchangeName
    ) {
        try{
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(apiService.fetchSingleCandlestickDataFromSelectedMarket(symbol, timeframe, exchangeName));
        } catch (final Exception exception) {
            log.error("Failed to fetch.", exception);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(exception);
        }
    }

    @GetMapping("/tickerDataFromAllExchanges")
    public ResponseEntity<Object> getTickerDataFromAllExchanges(
            @RequestParam final String symbol
    ) {
        try{
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(apiService.fetchTickerDataFromAllExchanges(symbol));
        } catch (final Exception exception) {
            log.error("Failed to fetch.", exception);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(exception);
        }
    }
}
