package com.slazic.CATER.controller;

import com.slazic.CATER.exception.EmailAlreadyInUseException;
import com.slazic.CATER.model.User;
import com.slazic.CATER.model.mapper.EntityDtoMapper;
import com.slazic.CATER.model.request.UserLoginRequestDto;
import com.slazic.CATER.model.request.UserRegisterRequestDto;
import com.slazic.CATER.service.EmailService;
import com.slazic.CATER.service.UserService;
import jakarta.mail.MessagingException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.mail.MailSendException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Slf4j
@RequestMapping("/authentication")
@RestController
public class AuthenticationController {

    private final UserService userService;

    private final EntityDtoMapper<UserRegisterRequestDto, User> userMapper;

    private final AuthenticationManager authenticationManager;

    private final EmailService emailService;

    @Autowired
    public AuthenticationController(
            @NotNull final UserService userService,
            @NotNull final EntityDtoMapper<UserRegisterRequestDto, User> userMapper,
            @NotNull final AuthenticationManager authenticationManager,
            @NotNull final EmailService emailService
    ) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationManager = authenticationManager;
        this.emailService = emailService;
    }

    @PostMapping("/register")
    public ResponseEntity<Object> register(
            @RequestBody @Valid @NotNull final UserRegisterRequestDto userRegisterRequest
    ) {
        try {
            userService.register(userMapper.map(userRegisterRequest));
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body("Successfully registered, you need to confirm email in 24 hours.");
        } catch (final EmailAlreadyInUseException exception) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body(exception.getMessage());
        } catch (final IllegalStateException | MailSendException exception) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("Email is not valid.");
        } catch (final Exception exception) {
            log.error("Failed to register user.", exception);

            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(exception);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(
            @RequestBody @Valid @NotNull final UserLoginRequestDto userLoginRequest
    ) throws MessagingException {
        User user = new User();
        try {
            user = userService.loadUserByEmail(userLoginRequest.getEmail());

            // Authenticates the user inside the security context
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            userLoginRequest.getEmail(),
                            userLoginRequest.getPassword()
                    )
            );

            return ResponseEntity
                    .status(HttpStatus.OK)
                    // Generates JWT token
                    .body(userService.authenticate(user));

        } catch (final BadCredentialsException | UsernameNotFoundException exception) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(userService.getInvalidCredentialsErrorMessage());
        } catch (final LockedException exception) {
            if(userService.isBlocked(user))
                return ResponseEntity
                        .status(HttpStatus.LOCKED)
                        .body("Account is locked.");


            if(!userService.canVerifyEmail(user.getCreatedAt()))
                emailService.resendVerificationEmail(user);

            return ResponseEntity
                    .status(HttpStatus.LOCKED)
                    .body(userService.getEmailNotVerifiedErrorMessage());
        } catch (final Exception exception) {
            log.error("Failed to login user.", exception);

            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(exception);
        }

    }

    @GetMapping("/verify")
    public ResponseEntity<Object> verify(
           @Nullable @RequestParam("hashed-identifier") String hash
    ) {
        try{
            userService.verify(hash);

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body("Account successfully verified.");
        } catch (final IllegalAccessException exception) {
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("Account is already verified.");
        } catch (final Exception exception) {
            log.error("Failed to verify Email.", exception);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(exception);
        }
    }

    /*@PostMapping("/logout")
    public ResponseEntity<Object> performLogout(
            Authentication authentication,
            HttpServletRequest request,
            HttpServletResponse response,
            SecurityContextLogoutHandler logoutHandler
    ) {
        try {
            logoutHandler.logout(request, response, authentication);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body("Successfully logged out.");
        } catch (Exception e) {
            log.error("Failed to log out.", e);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e);
        }
    }*/
}
