package com.slazic.CATER.utility;

import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public class Sha256Utility {

    public static @NotNull String getHash(@NotNull final String value) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("SHA3-256");
            byte[] encodedHash = digest.digest(value.getBytes(StandardCharsets.UTF_8));
            return bytesToHex(encodedHash);
        } catch (final NoSuchAlgorithmException exception){
            return value;
        }
    }

    public static @NotNull String bytesToHex(@NotNull final byte[] bytes){
        final StringBuilder hexStringBuilder = new StringBuilder();

        for(byte b : bytes)
            hexStringBuilder.append(String.format("%02x", b));

        return hexStringBuilder.toString();
    }
}
