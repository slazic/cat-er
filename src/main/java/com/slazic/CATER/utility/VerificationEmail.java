package com.slazic.CATER.utility;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class VerificationEmail {

    public static String buildEmail (String link, String value){
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional //EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html\n" +
                "  xmlns=\"http://www.w3.org/1999/xhtml\"\n" +
                "  xmlns:v=\"urn:schemas-microsoft-com:vml\"\n" +
                "  xmlns:o=\"urn:schemas-microsoft-com:office:office\"\n" +
                ">\n" +
                "  <head>\n" +
                "    <!--[if gte mso 9]>\n" +
                "      <xml>\n" +
                "        <o:OfficeDocumentSettings>\n" +
                "          <o:AllowPNG />\n" +
                "          <o:PixelsPerInch>96</o:PixelsPerInch>\n" +
                "        </o:OfficeDocumentSettings>\n" +
                "      </xml>\n" +
                "    <![endif]-->\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n" +
                "    <meta name=\"x-apple-disable-message-reformatting\" />\n" +
                "    <!--[if !mso]><!-->\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" +
                "    <!--<![endif]-->\n" +
                "    <title></title>\n" +
                "\n" +
                "    <style type=\"text/css\">\n" +
                "      @media only screen and (min-width: 620px) {\n" +
                "        .u-row {\n" +
                "          width: 600px !important;\n" +
                "        }\n" +
                "        .u-row .u-col {\n" +
                "          vertical-align: top;\n" +
                "        }\n" +
                "\n" +
                "        .u-row .u-col-100 {\n" +
                "          width: 600px !important;\n" +
                "        }\n" +
                "      }\n" +
                "\n" +
                "      @media (max-width: 620px) {\n" +
                "        .u-row-container {\n" +
                "          max-width: 100% !important;\n" +
                "          padding-left: 0px !important;\n" +
                "          padding-right: 0px !important;\n" +
                "        }\n" +
                "        .u-row .u-col {\n" +
                "          min-width: 320px !important;\n" +
                "          max-width: 100% !important;\n" +
                "          display: block !important;\n" +
                "        }\n" +
                "        .u-row {\n" +
                "          width: 100% !important;\n" +
                "        }\n" +
                "        .u-col {\n" +
                "          width: 100% !important;\n" +
                "        }\n" +
                "        .u-col > div {\n" +
                "          margin: 0 auto;\n" +
                "        }\n" +
                "      }\n" +
                "      body {\n" +
                "        margin: 0;\n" +
                "        padding: 0;\n" +
                "      }\n" +
                "\n" +
                "      table,\n" +
                "      tr,\n" +
                "      td {\n" +
                "        vertical-align: top;\n" +
                "        border-collapse: collapse;\n" +
                "      }\n" +
                "\n" +
                "      p {\n" +
                "        margin: 0;\n" +
                "      }\n" +
                "\n" +
                "      .ie-container table,\n" +
                "      .mso-container table {\n" +
                "        table-layout: fixed;\n" +
                "      }\n" +
                "\n" +
                "      * {\n" +
                "        line-height: inherit;\n" +
                "      }\n" +
                "\n" +
                "      a[x-apple-data-detectors=\"true\"] {\n" +
                "        color: inherit !important;\n" +
                "        text-decoration: none !important;\n" +
                "      }\n" +
                "\n" +
                "      table,\n" +
                "      td {\n" +
                "        color: #000000;\n" +
                "      }\n" +
                "      #u_body a {\n" +
                "        color: #0000ee;\n" +
                "        text-decoration: underline;\n" +
                "      }\n" +
                "      @media (max-width: 480px) {\n" +
                "        #u_content_image_4 .v-src-width {\n" +
                "          width: auto !important;\n" +
                "        }\n" +
                "        #u_content_image_4 .v-src-max-width {\n" +
                "          max-width: 43% !important;\n" +
                "        }\n" +
                "        #u_content_heading_1 .v-container-padding-padding {\n" +
                "          padding: 8px 20px 0px !important;\n" +
                "        }\n" +
                "        #u_content_heading_1 .v-font-size {\n" +
                "          font-size: 21px !important;\n" +
                "        }\n" +
                "        #u_content_heading_1 .v-text-align {\n" +
                "          text-align: center !important;\n" +
                "        }\n" +
                "        #u_content_text_2 .v-container-padding-padding {\n" +
                "          padding: 35px 15px 10px !important;\n" +
                "        }\n" +
                "        #u_content_text_3 .v-container-padding-padding {\n" +
                "          padding: 10px 15px 40px !important;\n" +
                "        }\n" +
                "      }\n" +
                "    </style>\n" +
                "\n" +
                "    <!--[if !mso]><!-->\n" +
                "    <link\n" +
                "      href=\"https://fonts.googleapis.com/css?family=Lato:400,700&display=swap\"\n" +
                "      rel=\"stylesheet\"\n" +
                "      type=\"text/css\"\n" +
                "    />\n" +
                "    <link\n" +
                "      href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap\"\n" +
                "      rel=\"stylesheet\"\n" +
                "      type=\"text/css\"\n" +
                "    />\n" +
                "    <!--<![endif]-->\n" +
                "  </head>\n" +
                "\n" +
                "  <body\n" +
                "    class=\"clean-body u_body\"\n" +
                "    style=\"\n" +
                "      margin: 0;\n" +
                "      padding: 0;\n" +
                "      -webkit-text-size-adjust: 100%;\n" +
                "      background-color: #c2e0f4;\n" +
                "      color: #000000;\n" +
                "    \"\n" +
                "  >\n" +
                "    <!--[if IE]><div class=\"ie-container\"><![endif]-->\n" +
                "    <!--[if mso]><div class=\"mso-container\"><![endif]-->\n" +
                "    <table\n" +
                "      id=\"u_body\"\n" +
                "      style=\"\n" +
                "        border-collapse: collapse;\n" +
                "        table-layout: fixed;\n" +
                "        border-spacing: 0;\n" +
                "        mso-table-lspace: 0pt;\n" +
                "        mso-table-rspace: 0pt;\n" +
                "        vertical-align: top;\n" +
                "        min-width: 320px;\n" +
                "        margin: 0 auto;\n" +
                "        background-color: #c2e0f4;\n" +
                "        width: 100%;\n" +
                "      \"\n" +
                "      cellpadding=\"0\"\n" +
                "      cellspacing=\"0\"\n" +
                "    >\n" +
                "      <tbody>\n" +
                "        <tr style=\"vertical-align: top\">\n" +
                "          <td\n" +
                "            style=\"\n" +
                "              word-break: break-word;\n" +
                "              border-collapse: collapse !important;\n" +
                "              vertical-align: top;\n" +
                "            \"\n" +
                "          >\n" +
                "            <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td align=\"center\" style=\"background-color: #c2e0f4;\"><![endif]-->\n" +
                "\n" +
                "            <div\n" +
                "              class=\"u-row-container\"\n" +
                "              style=\"padding: 0px; background-color: transparent\"\n" +
                "            >\n" +
                "              <div\n" +
                "                class=\"u-row\"\n" +
                "                style=\"\n" +
                "                  margin: 0 auto;\n" +
                "                  min-width: 320px;\n" +
                "                  max-width: 600px;\n" +
                "                  overflow-wrap: break-word;\n" +
                "                  word-wrap: break-word;\n" +
                "                  word-break: break-word;\n" +
                "                  background-color: #ffffff;\n" +
                "                \"\n" +
                "              >\n" +
                "                <div\n" +
                "                  style=\"\n" +
                "                    border-collapse: collapse;\n" +
                "                    display: table;\n" +
                "                    width: 100%;\n" +
                "                    height: 100%;\n" +
                "                    background-color: transparent;\n" +
                "                  \"\n" +
                "                >\n" +
                "                  <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding: 0px;background-color: transparent;\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:600px;\"><tr style=\"background-color: #ffffff;\"><![endif]-->\n" +
                "\n" +
                "                  <!--[if (mso)|(IE)]><td align=\"center\" width=\"600\" style=\"width: 600px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;\" valign=\"top\"><![endif]-->\n" +
                "                  <div\n" +
                "                    class=\"u-col u-col-100\"\n" +
                "                    style=\"\n" +
                "                      max-width: 320px;\n" +
                "                      min-width: 600px;\n" +
                "                      display: table-cell;\n" +
                "                      vertical-align: top;\n" +
                "                    \"\n" +
                "                  >\n" +
                "                    <div style=\"height: 100%; width: 100% !important\">\n" +
                "                      <!--[if (!mso)&(!IE)]><!--><div\n" +
                "                        style=\"\n" +
                "                          box-sizing: border-box;\n" +
                "                          height: 100%;\n" +
                "                          padding: 0px;\n" +
                "                          border-top: 0px solid transparent;\n" +
                "                          border-left: 0px solid transparent;\n" +
                "                          border-right: 0px solid transparent;\n" +
                "                          border-bottom: 0px solid transparent;\n" +
                "                        \"\n" +
                "                      ><!--<![endif]-->\n" +
                "                        <table\n" +
                "                          style=\"font-family: arial, helvetica, sans-serif\"\n" +
                "                          role=\"presentation\"\n" +
                "                          cellpadding=\"0\"\n" +
                "                          cellspacing=\"0\"\n" +
                "                          width=\"100%\"\n" +
                "                          border=\"0\"\n" +
                "                        >\n" +
                "                          <tbody>\n" +
                "                            <tr>\n" +
                "                              <td\n" +
                "                                class=\"v-container-padding-padding\"\n" +
                "                                style=\"\n" +
                "                                  overflow-wrap: break-word;\n" +
                "                                  word-break: break-word;\n" +
                "                                  padding: 0px 0px 10px;\n" +
                "                                  font-family: arial, helvetica, sans-serif;\n" +
                "                                \"\n" +
                "                                align=\"left\"\n" +
                "                              >\n" +
                "                                <table\n" +
                "                                  height=\"0px\"\n" +
                "                                  align=\"center\"\n" +
                "                                  border=\"0\"\n" +
                "                                  cellpadding=\"0\"\n" +
                "                                  cellspacing=\"0\"\n" +
                "                                  width=\"100%\"\n" +
                "                                  style=\"\n" +
                "                                    border-collapse: collapse;\n" +
                "                                    table-layout: fixed;\n" +
                "                                    border-spacing: 0;\n" +
                "                                    mso-table-lspace: 0pt;\n" +
                "                                    mso-table-rspace: 0pt;\n" +
                "                                    vertical-align: top;\n" +
                "                                    border-top: 6px solid #6f9de1;\n" +
                "                                    -ms-text-size-adjust: 100%;\n" +
                "                                    -webkit-text-size-adjust: 100%;\n" +
                "                                  \"\n" +
                "                                >\n" +
                "                                  <tbody>\n" +
                "                                    <tr style=\"vertical-align: top\">\n" +
                "                                      <td\n" +
                "                                        style=\"\n" +
                "                                          word-break: break-word;\n" +
                "                                          border-collapse: collapse !important;\n" +
                "                                          vertical-align: top;\n" +
                "                                          font-size: 0px;\n" +
                "                                          line-height: 0px;\n" +
                "                                          mso-line-height-rule: exactly;\n" +
                "                                          -ms-text-size-adjust: 100%;\n" +
                "                                          -webkit-text-size-adjust: 100%;\n" +
                "                                        \"\n" +
                "                                      >\n" +
                "                                        <span>&#160;</span>\n" +
                "                                      </td>\n" +
                "                                    </tr>\n" +
                "                                  </tbody>\n" +
                "                                </table>\n" +
                "                              </td>\n" +
                "                            </tr>\n" +
                "                          </tbody>\n" +
                "                        </table>\n" +
                "\n" +
                "                        <table\n" +
                "                          id=\"u_content_image_4\"\n" +
                "                          style=\"font-family: arial, helvetica, sans-serif\"\n" +
                "                          role=\"presentation\"\n" +
                "                          cellpadding=\"0\"\n" +
                "                          cellspacing=\"0\"\n" +
                "                          width=\"100%\"\n" +
                "                          border=\"0\"\n" +
                "                        >\n" +
                "                          <tbody>\n" +
                "                            <tr>\n" +
                "                              <td\n" +
                "                                class=\"v-container-padding-padding\"\n" +
                "                                style=\"\n" +
                "                                  overflow-wrap: break-word;\n" +
                "                                  word-break: break-word;\n" +
                "                                  padding: 20px 10px;\n" +
                "                                  font-family: arial, helvetica, sans-serif;\n" +
                "                                \"\n" +
                "                                align=\"left\"\n" +
                "                              >\n" +
                "                                <table\n" +
                "                                  width=\"100%\"\n" +
                "                                  cellpadding=\"0\"\n" +
                "                                  cellspacing=\"0\"\n" +
                "                                  border=\"0\"\n" +
                "                                >\n" +
                "                                  <tr>\n" +
                "                                    <td\n" +
                "                                      class=\"v-text-align\"\n" +
                "                                      style=\"\n" +
                "                                        padding-right: 0px;\n" +
                "                                        padding-left: 0px;\n" +
                "                                      \"\n" +
                "                                      align=\"center\"\n" +
                "                                    >\n" +
                "                                      <img\n" +
                "                                        align=\"center\"\n" +
                "                                        border=\"0\"\n" +
                "                                        src=\"https://i.imgur.com/IBwhO7f.png\"\n" +
                "                                        alt=\"Logo\"\n" +
                "                                        title=\"Logo\"\n" +
                "                                        style=\"\n" +
                "                                          outline: none;\n" +
                "                                          text-decoration: none;\n" +
                "                                          -ms-interpolation-mode: bicubic;\n" +
                "                                          clear: both;\n" +
                "                                          display: inline-block !important;\n" +
                "                                          border: none;\n" +
                "                                          height: auto;\n" +
                "                                          float: none;\n" +
                "                                          width: 30%;\n" +
                "                                          max-width: 174px;\n" +
                "                                        \"\n" +
                "                                        width=\"174\"\n" +
                "                                        class=\"v-src-width v-src-max-width\"\n" +
                "                                      />\n" +
                "                                    </td>\n" +
                "                                  </tr>\n" +
                "                                </table>\n" +
                "                              </td>\n" +
                "                            </tr>\n" +
                "                          </tbody>\n" +
                "                        </table>\n" +
                "\n" +
                "                        <!--[if (!mso)&(!IE)]><!-->\n" +
                "                      </div>\n" +
                "                      <!--<![endif]-->\n" +
                "                    </div>\n" +
                "                  </div>\n" +
                "                  <!--[if (mso)|(IE)]></td><![endif]-->\n" +
                "                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->\n" +
                "                </div>\n" +
                "              </div>\n" +
                "            </div>\n" +
                "\n" +
                "            <div\n" +
                "              class=\"u-row-container\"\n" +
                "              style=\"padding: 0px; background-color: transparent\"\n" +
                "            >\n" +
                "              <div\n" +
                "                class=\"u-row\"\n" +
                "                style=\"\n" +
                "                  margin: 0 auto;\n" +
                "                  min-width: 320px;\n" +
                "                  max-width: 600px;\n" +
                "                  overflow-wrap: break-word;\n" +
                "                  word-wrap: break-word;\n" +
                "                  word-break: break-word;\n" +
                "                  background-color: #ffffff;\n" +
                "                \"\n" +
                "              >\n" +
                "                <div\n" +
                "                  style=\"\n" +
                "                    border-collapse: collapse;\n" +
                "                    display: table;\n" +
                "                    width: 100%;\n" +
                "                    height: 100%;\n" +
                "                    background-color: transparent;\n" +
                "                  \"\n" +
                "                >\n" +
                "                  <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding: 0px;background-color: transparent;\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:600px;\"><tr style=\"background-color: #ffffff;\"><![endif]-->\n" +
                "\n" +
                "                  <!--[if (mso)|(IE)]><td align=\"center\" width=\"600\" style=\"width: 600px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;\" valign=\"top\"><![endif]-->\n" +
                "                  <div\n" +
                "                    class=\"u-col u-col-100\"\n" +
                "                    style=\"\n" +
                "                      max-width: 320px;\n" +
                "                      min-width: 600px;\n" +
                "                      display: table-cell;\n" +
                "                      vertical-align: top;\n" +
                "                    \"\n" +
                "                  >\n" +
                "                    <div\n" +
                "                      style=\"\n" +
                "                        height: 100%;\n" +
                "                        width: 100% !important;\n" +
                "                        border-radius: 0px;\n" +
                "                        -webkit-border-radius: 0px;\n" +
                "                        -moz-border-radius: 0px;\n" +
                "                      \"\n" +
                "                    >\n" +
                "                      <!--[if (!mso)&(!IE)]><!--><div\n" +
                "                        style=\"\n" +
                "                          box-sizing: border-box;\n" +
                "                          height: 100%;\n" +
                "                          padding: 0px;\n" +
                "                          border-top: 0px solid transparent;\n" +
                "                          border-left: 0px solid transparent;\n" +
                "                          border-right: 0px solid transparent;\n" +
                "                          border-bottom: 0px solid transparent;\n" +
                "                          border-radius: 0px;\n" +
                "                          -webkit-border-radius: 0px;\n" +
                "                          -moz-border-radius: 0px;\n" +
                "                        \"\n" +
                "                      ><!--<![endif]-->\n" +
                "                        <table\n" +
                "                          style=\"font-family: arial, helvetica, sans-serif\"\n" +
                "                          role=\"presentation\"\n" +
                "                          cellpadding=\"0\"\n" +
                "                          cellspacing=\"0\"\n" +
                "                          width=\"100%\"\n" +
                "                          border=\"0\"\n" +
                "                        >\n" +
                "                          <tbody>\n" +
                "                            <tr>\n" +
                "                              <td\n" +
                "                                class=\"v-container-padding-padding\"\n" +
                "                                style=\"\n" +
                "                                  overflow-wrap: break-word;\n" +
                "                                  word-break: break-word;\n" +
                "                                  padding: 10px;\n" +
                "                                  font-family: arial, helvetica, sans-serif;\n" +
                "                                \"\n" +
                "                                align=\"left\"\n" +
                "                              >\n" +
                "                                <table\n" +
                "                                  width=\"100%\"\n" +
                "                                  cellpadding=\"0\"\n" +
                "                                  cellspacing=\"0\"\n" +
                "                                  border=\"0\"\n" +
                "                                >\n" +
                "                                  <tr>\n" +
                "                                    <td\n" +
                "                                      class=\"v-text-align\"\n" +
                "                                      style=\"\n" +
                "                                        padding-right: 0px;\n" +
                "                                        padding-left: 0px;\n" +
                "                                      \"\n" +
                "                                      align=\"center\"\n" +
                "                                    >\n" +
                "                                      <img\n" +
                "                                        align=\"center\"\n" +
                "                                        border=\"0\"\n" +
                "                                        src=\"https://i.imgur.com/MBugwAT.png\"\n" +
                "                                        alt=\"Banner\"\n" +
                "                                        title=\"Banner\"\n" +
                "                                        style=\"\n" +
                "                                          outline: none;\n" +
                "                                          text-decoration: none;\n" +
                "                                          -ms-interpolation-mode: bicubic;\n" +
                "                                          clear: both;\n" +
                "                                          display: inline-block !important;\n" +
                "                                          border: none;\n" +
                "                                          height: auto;\n" +
                "                                          float: none;\n" +
                "                                          width: 94%;\n" +
                "                                          max-width: 545.2px;\n" +
                "                                        \"\n" +
                "                                        width=\"545.2\"\n" +
                "                                        class=\"v-src-width v-src-max-width\"\n" +
                "                                      />\n" +
                "                                    </td>\n" +
                "                                  </tr>\n" +
                "                                </table>\n" +
                "                              </td>\n" +
                "                            </tr>\n" +
                "                          </tbody>\n" +
                "                        </table>\n" +
                "\n" +
                "                        <table\n" +
                "                          id=\"u_content_heading_1\"\n" +
                "                          style=\"font-family: arial, helvetica, sans-serif\"\n" +
                "                          role=\"presentation\"\n" +
                "                          cellpadding=\"0\"\n" +
                "                          cellspacing=\"0\"\n" +
                "                          width=\"100%\"\n" +
                "                          border=\"0\"\n" +
                "                        >\n" +
                "                          <tbody>\n" +
                "                            <tr>\n" +
                "                              <td\n" +
                "                                class=\"v-container-padding-padding\"\n" +
                "                                style=\"\n" +
                "                                  overflow-wrap: break-word;\n" +
                "                                  word-break: break-word;\n" +
                "                                  padding: 9px 30px 40px 31px;\n" +
                "                                  font-family: arial, helvetica, sans-serif;\n" +
                "                                \"\n" +
                "                                align=\"left\"\n" +
                "                              >\n" +
                "                                <h1\n" +
                "                                  class=\"v-text-align v-font-size\"\n" +
                "                                  style=\"\n" +
                "                                    margin: 0px;\n" +
                "                                    color: #023047;\n" +
                "                                    line-height: 170%;\n" +
                "                                    text-align: center;\n" +
                "                                    word-wrap: break-word;\n" +
                "                                    font-family: 'Open Sans', sans-serif;\n" +
                "                                    font-size: 26px;\n" +
                "                                  \"\n" +
                "                                >\n" +
                "                                  <strong\n" +
                "                                    >This is verification e-mail from: <br>\n" +
                "                                    CAT-ER</strong\n" +
                "                                  >\n" +
                "                                </h1>\n" +
                "                              </td>\n" +
                "                            </tr>\n" +
                "                          </tbody>\n" +
                "                        </table>\n" +
                "\n" +
                "                        <!--[if (!mso)&(!IE)]><!-->\n" +
                "                      </div>\n" +
                "                      <!--<![endif]-->\n" +
                "                    </div>\n" +
                "                  </div>\n" +
                "                  <!--[if (mso)|(IE)]></td><![endif]-->\n" +
                "                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->\n" +
                "                </div>\n" +
                "              </div>\n" +
                "            </div>\n" +
                "\n" +
                "            <div\n" +
                "              class=\"u-row-container\"\n" +
                "              style=\"padding: 0px; background-color: transparent\"\n" +
                "            >\n" +
                "              <div\n" +
                "                class=\"u-row\"\n" +
                "                style=\"\n" +
                "                  margin: 0 auto;\n" +
                "                  min-width: 320px;\n" +
                "                  max-width: 600px;\n" +
                "                  overflow-wrap: break-word;\n" +
                "                  word-wrap: break-word;\n" +
                "                  word-break: break-word;\n" +
                "                  background-color: #ffffff;\n" +
                "                \"\n" +
                "              >\n" +
                "                <div\n" +
                "                  style=\"\n" +
                "                    border-collapse: collapse;\n" +
                "                    display: table;\n" +
                "                    width: 100%;\n" +
                "                    height: 100%;\n" +
                "                    background-color: transparent;\n" +
                "                  \"\n" +
                "                >\n" +
                "                  <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding: 0px;background-color: transparent;\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:600px;\"><tr style=\"background-color: #ffffff;\"><![endif]-->\n" +
                "\n" +
                "                  <!--[if (mso)|(IE)]><td align=\"center\" width=\"600\" style=\"width: 600px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;\" valign=\"top\"><![endif]-->\n" +
                "                  <div\n" +
                "                    class=\"u-col u-col-100\"\n" +
                "                    style=\"\n" +
                "                      max-width: 320px;\n" +
                "                      min-width: 600px;\n" +
                "                      display: table-cell;\n" +
                "                      vertical-align: top;\n" +
                "                    \"\n" +
                "                  >\n" +
                "                    <div\n" +
                "                      style=\"\n" +
                "                        height: 100%;\n" +
                "                        width: 100% !important;\n" +
                "                        border-radius: 0px;\n" +
                "                        -webkit-border-radius: 0px;\n" +
                "                        -moz-border-radius: 0px;\n" +
                "                      \"\n" +
                "                    >\n" +
                "                      <!--[if (!mso)&(!IE)]><!--><div\n" +
                "                        style=\"\n" +
                "                          box-sizing: border-box;\n" +
                "                          height: 100%;\n" +
                "                          padding: 0px;\n" +
                "                          border-top: 0px solid transparent;\n" +
                "                          border-left: 0px solid transparent;\n" +
                "                          border-right: 0px solid transparent;\n" +
                "                          border-bottom: 0px solid transparent;\n" +
                "                          border-radius: 0px;\n" +
                "                          -webkit-border-radius: 0px;\n" +
                "                          -moz-border-radius: 0px;\n" +
                "                        \"\n" +
                "                      ><!--<![endif]-->\n" +
                "                        <table\n" +
                "                          id=\"u_content_text_2\"\n" +
                "                          style=\"font-family: arial, helvetica, sans-serif\"\n" +
                "                          role=\"presentation\"\n" +
                "                          cellpadding=\"0\"\n" +
                "                          cellspacing=\"0\"\n" +
                "                          width=\"100%\"\n" +
                "                          border=\"0\"\n" +
                "                        >\n" +
                "                          <tbody>\n" +
                "                            <tr>\n" +
                "                              <td\n" +
                "                                class=\"v-container-padding-padding\"\n" +
                "                                style=\"\n" +
                "                                  overflow-wrap: break-word;\n" +
                "                                  word-break: break-word;\n" +
                "                                  padding: 35px 55px 10px;\n" +
                "                                  font-family: arial, helvetica, sans-serif;\n" +
                "                                \"\n" +
                "                                align=\"left\"\n" +
                "                              >\n" +
                "                                <div\n" +
                "                                  class=\"v-text-align v-font-size\"\n" +
                "                                  style=\"\n" +
                "                                    color: #333333;\n" +
                "                                    line-height: 180%;\n" +
                "                                    text-align: left;\n" +
                "                                    word-wrap: break-word;\n" +
                "                                  \"\n" +
                "                                >\n" +
                "                                  <p style=\"font-size: 14px; line-height: 180%\">\n" +
                "                                    <span\n" +
                "                                      style=\"\n" +
                "                                        font-size: 18px;\n" +
                "                                        line-height: 32.4px;\n" +
                "                                        font-family: Lato, sans-serif;\n" +
                "                                      \"\n" +
                "                                      ><strong\n" +
                "                                        ><span\n" +
                "                                          style=\"\n" +
                "                                            line-height: 32.4px;\n" +
                "                                            font-size: 18px;\n" +
                "                                          \"\n" +
                "                                          >Hi, </span\n" +
                "                                        ></strong\n" +
                "                                      ></span\n" +
                "                                    >\n" +
                "                                  </p>\n" +
                "                                  <p style=\"font-size: 14px; line-height: 180%\">\n" +
                "                                     \n" +
                "                                  </p>\n" +
                "                                  <p style=\"font-size: 14px; line-height: 180%\">\n" +
                "                                    <span\n" +
                "                                      style=\"\n" +
                "                                        font-family: Lato, sans-serif;\n" +
                "                                        font-size: 16px;\n" +
                "                                        line-height: 28.8px;\n" +
                "                                      \"\n" +
                "                                      >We received your registration request and\n" +
                "                                      hope this mail reads you<br /> well.\n" +
                "                                      Acquisition often requires a huge\n" +
                "                                      investment of time and<br /> resources, so\n" +
                "                                      it is always safe and secure to do your\n" +
                "                                      due diligence<br /> before boarding a new\n" +
                "                                      API accessor.</span\n" +
                "                                    >\n" +
                "                                  </p>\n" +
                "                                  <p style=\"font-size: 14px; line-height: 180%\">\n" +
                "                                     \n" +
                "                                  </p>\n" +
                "                                  <p style=\"line-height: 180%\">\n" +
                "                                    <strong\n" +
                "                                      >Please click the button below to verify\n" +
                "                                      your account.</strong\n" +
                "                                    >\n" +
                "                                  </p>\n" +
                "                                </div>\n" +
                "                              </td>\n" +
                "                            </tr>\n" +
                "                          </tbody>\n" +
                "                        </table>\n" +
                "\n" +
                "                        <table\n" +
                "                          style=\"font-family: arial, helvetica, sans-serif\"\n" +
                "                          role=\"presentation\"\n" +
                "                          cellpadding=\"0\"\n" +
                "                          cellspacing=\"0\"\n" +
                "                          width=\"100%\"\n" +
                "                          border=\"0\"\n" +
                "                        >\n" +
                "                          <tbody>\n" +
                "                            <tr>\n" +
                "                              <td\n" +
                "                                class=\"v-container-padding-padding\"\n" +
                "                                style=\"\n" +
                "                                  overflow-wrap: break-word;\n" +
                "                                  word-break: break-word;\n" +
                "                                  padding: 20px 10px 30px;\n" +
                "                                  font-family: arial, helvetica, sans-serif;\n" +
                "                                \"\n" +
                "                                align=\"left\"\n" +
                "                              >\n" +
                "                                <!--[if mso\n" +
                "                                  ]><style>\n" +
                "                                    .v-button {\n" +
                "                                      background: transparent !important;\n" +
                "                                    }\n" +
                "                                  </style><!\n" +
                "                                [endif]-->\n" +
                "                                <div class=\"v-text-align\" align=\"center\">\n" +
                "                                  <!--[if mso]><v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"https://unlayer.com\" style=\"height:57px; v-text-anchor:middle; width:264px;\" arcsize=\"77%\"  stroke=\"f\" fillcolor=\"#33428d\"><w:anchorlock/><center style=\"color:#FFFFFF;font-family:arial,helvetica,sans-serif;\"><![endif]-->\n" +
                "                                  <a\n" +
                "                                    href=\""+ link + value+"\"\n" +
                "                                    target=\"_blank\"\n" +
                "                                    class=\"v-button v-font-size\"\n" +
                "                                    style=\"\n" +
                "                                    box-sizing: border-box;\n" +
                "                                    display: inline-block;\n" +
                "                                    font-family: arial, helvetica, sans-serif;\n" +
                "                                    text-decoration: none;\n" +
                "                                    -webkit-text-size-adjust: none;\n" +
                "                                    text-align: center;\n" +
                "                                    color: #ffffff;\n" +
                "                                    background-color: #33428d;\n" +
                "                                    border-radius: 44px;\n" +
                "                                    -webkit-border-radius: 44px;\n" +
                "                                    -moz-border-radius: 44px;\n" +
                "                                    width: auto;\n" +
                "                                    max-width: 100%;\n" +
                "                                    overflow-wrap: break-word;\n" +
                "                                    word-break: break-word;\n" +
                "                                    word-wrap: break-word;\n" +
                "                                    mso-border-alt: none;\n" +
                "                                    font-size: 14px;\n" +
                "                                    \"\n" +
                "                                  >\n" +
                "                                    <span\n" +
                "                                      style=\"\n" +
                "                                        display: block;\n" +
                "                                        padding: 20px 70px;\n" +
                "                                        line-height: 120%;\n" +
                "                                      \"\n" +
                "                                      ><strong\n" +
                "                                        ><span style=\"line-height: 16.8px\"\n" +
                "                                          >V E R I F Y   N O W</span\n" +
                "                                        ></strong\n" +
                "                                      ></span\n" +
                "                                    >\n" +
                "                                  </a>\n" +
                "                                  <!--[if mso]></center></v:roundrect><![endif]-->\n" +
                "                                </div>\n" +
                "                              </td>\n" +
                "                            </tr>\n" +
                "                          </tbody>\n" +
                "                        </table>\n" +
                "\n" +
                "                        <table\n" +
                "                          id=\"u_content_text_3\"\n" +
                "                          style=\"font-family: arial, helvetica, sans-serif\"\n" +
                "                          role=\"presentation\"\n" +
                "                          cellpadding=\"0\"\n" +
                "                          cellspacing=\"0\"\n" +
                "                          width=\"100%\"\n" +
                "                          border=\"0\"\n" +
                "                        >\n" +
                "                          <tbody>\n" +
                "                            <tr>\n" +
                "                              <td\n" +
                "                                class=\"v-container-padding-padding\"\n" +
                "                                style=\"\n" +
                "                                  overflow-wrap: break-word;\n" +
                "                                  word-break: break-word;\n" +
                "                                  padding: 10px 55px 40px;\n" +
                "                                  font-family: arial, helvetica, sans-serif;\n" +
                "                                \"\n" +
                "                                align=\"left\"\n" +
                "                              >\n" +
                "                                <div\n" +
                "                                  class=\"v-text-align v-font-size\"\n" +
                "                                  style=\"\n" +
                "                                    line-height: 170%;\n" +
                "                                    text-align: left;\n" +
                "                                    word-wrap: break-word;\n" +
                "                                  \"\n" +
                "                                >\n" +
                "                                  <p style=\"font-size: 14px; line-height: 170%\">\n" +
                "                                    <span\n" +
                "                                      style=\"\n" +
                "                                        font-family: Lato, sans-serif;\n" +
                "                                        font-size: 16px;\n" +
                "                                        line-height: 27.2px;\n" +
                "                                      \"\n" +
                "                                      >This will ensure the authenticity of your\n" +
                "                                      data and information with the verification\n" +
                "                                      process is pretty simple and short. The\n" +
                "                                      information you provide is confidential\n" +
                "                                      and safe with us. </span\n" +
                "                                    >\n" +
                "                                  </p>\n" +
                "                                  <p style=\"font-size: 14px; line-height: 170%\">\n" +
                "                                     \n" +
                "                                  </p>\n" +
                "                                  <p style=\"font-size: 14px; line-height: 170%\">\n" +
                "                                    <span\n" +
                "                                      style=\"\n" +
                "                                        font-family: Lato, sans-serif;\n" +
                "                                        font-size: 16px;\n" +
                "                                        line-height: 27.2px;\n" +
                "                                      \"\n" +
                "                                      >If you have any questions/issues\n" +
                "                                      regarding the process, feel free to\n" +
                "                                      contact us. <br /><br /><strong\n" +
                "                                        >Email expires within 24 hours.</strong\n" +
                "                                      ><br\n" +
                "                                    /></span>\n" +
                "                                  </p>\n" +
                "                                  <p style=\"font-size: 14px; line-height: 170%\">\n" +
                "                                     \n" +
                "                                  </p>\n" +
                "                                  <p style=\"font-size: 14px; line-height: 170%\">\n" +
                "                                    <span\n" +
                "                                      style=\"\n" +
                "                                        font-family: Lato, sans-serif;\n" +
                "                                        font-size: 16px;\n" +
                "                                        line-height: 27.2px;\n" +
                "                                      \"\n" +
                "                                      >With Regards,</span\n" +
                "                                    >\n" +
                "                                  </p>\n" +
                "                                  <p style=\"font-size: 14px; line-height: 170%\">\n" +
                "                                    <span\n" +
                "                                      style=\"\n" +
                "                                        font-family: Lato, sans-serif;\n" +
                "                                        font-size: 14px;\n" +
                "                                        line-height: 23.8px;\n" +
                "                                      \"\n" +
                "                                      ><strong\n" +
                "                                        ><span\n" +
                "                                          style=\"\n" +
                "                                            font-size: 16px;\n" +
                "                                            line-height: 27.2px;\n" +
                "                                          \"\n" +
                "                                          >CAT-ER<br /></span></strong\n" +
                "                                    ></span>\n" +
                "                                  </p>\n" +
                "                                </div>\n" +
                "                              </td>\n" +
                "                            </tr>\n" +
                "                          </tbody>\n" +
                "                        </table>\n" +
                "\n" +
                "                        <!--[if (!mso)&(!IE)]><!-->\n" +
                "                      </div>\n" +
                "                      <!--<![endif]-->\n" +
                "                    </div>\n" +
                "                  </div>\n" +
                "                  <!--[if (mso)|(IE)]></td><![endif]-->\n" +
                "                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->\n" +
                "                </div>\n" +
                "              </div>\n" +
                "            </div>\n" +
                "\n" +
                "            <div\n" +
                "              class=\"u-row-container\"\n" +
                "              style=\"padding: 0px; background-color: transparent\"\n" +
                "            >\n" +
                "              <div\n" +
                "                class=\"u-row\"\n" +
                "                style=\"\n" +
                "                  margin: 0 auto;\n" +
                "                  min-width: 320px;\n" +
                "                  max-width: 600px;\n" +
                "                  overflow-wrap: break-word;\n" +
                "                  word-wrap: break-word;\n" +
                "                  word-break: break-word;\n" +
                "                  background-color: #ffffff;\n" +
                "                \"\n" +
                "              >\n" +
                "                <div\n" +
                "                  style=\"\n" +
                "                    border-collapse: collapse;\n" +
                "                    display: table;\n" +
                "                    width: 100%;\n" +
                "                    height: 100%;\n" +
                "                    background-color: transparent;\n" +
                "                  \"\n" +
                "                >\n" +
                "                  <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding: 0px;background-color: transparent;\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:600px;\"><tr style=\"background-color: #ffffff;\"><![endif]-->\n" +
                "\n" +
                "                  <!--[if (mso)|(IE)]><td align=\"center\" width=\"600\" style=\"width: 600px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;\" valign=\"top\"><![endif]-->\n" +
                "                  <div\n" +
                "                    class=\"u-col u-col-100\"\n" +
                "                    style=\"\n" +
                "                      max-width: 320px;\n" +
                "                      min-width: 600px;\n" +
                "                      display: table-cell;\n" +
                "                      vertical-align: top;\n" +
                "                    \"\n" +
                "                  >\n" +
                "                    <div\n" +
                "                      style=\"\n" +
                "                        height: 100%;\n" +
                "                        width: 100% !important;\n" +
                "                        border-radius: 0px;\n" +
                "                        -webkit-border-radius: 0px;\n" +
                "                        -moz-border-radius: 0px;\n" +
                "                      \"\n" +
                "                    >\n" +
                "                      <!--[if (!mso)&(!IE)]><!--><div\n" +
                "                        style=\"\n" +
                "                          box-sizing: border-box;\n" +
                "                          height: 100%;\n" +
                "                          padding: 0px;\n" +
                "                          border-top: 0px solid transparent;\n" +
                "                          border-left: 0px solid transparent;\n" +
                "                          border-right: 0px solid transparent;\n" +
                "                          border-bottom: 0px solid transparent;\n" +
                "                          border-radius: 0px;\n" +
                "                          -webkit-border-radius: 0px;\n" +
                "                          -moz-border-radius: 0px;\n" +
                "                        \"\n" +
                "                      ><!--<![endif]-->\n" +
                "                        <table\n" +
                "                          style=\"font-family: arial, helvetica, sans-serif\"\n" +
                "                          role=\"presentation\"\n" +
                "                          cellpadding=\"0\"\n" +
                "                          cellspacing=\"0\"\n" +
                "                          width=\"100%\"\n" +
                "                          border=\"0\"\n" +
                "                        >\n" +
                "                          <tbody>\n" +
                "                            <tr>\n" +
                "                              <td\n" +
                "                                class=\"v-container-padding-padding\"\n" +
                "                                style=\"\n" +
                "                                  overflow-wrap: break-word;\n" +
                "                                  word-break: break-word;\n" +
                "                                  padding: 5px 10px 40px;\n" +
                "                                  font-family: arial, helvetica, sans-serif;\n" +
                "                                \"\n" +
                "                                align=\"left\"\n" +
                "                              >\n" +
                "                                <h1\n" +
                "                                  class=\"v-text-align v-font-size\"\n" +
                "                                  style=\"\n" +
                "                                    margin: 0px;\n" +
                "                                    color: #000000;\n" +
                "                                    line-height: 140%;\n" +
                "                                    text-align: center;\n" +
                "                                    word-wrap: break-word;\n" +
                "                                    font-family: 'Lato', sans-serif;\n" +
                "                                    font-size: 26px;\n" +
                "                                  \"\n" +
                "                                >\n" +
                "                                  slazic@etfos.hr\n" +
                "                                </h1>\n" +
                "                              </td>\n" +
                "                            </tr>\n" +
                "                          </tbody>\n" +
                "                        </table>\n" +
                "\n" +
                "                        <!--[if (!mso)&(!IE)]><!-->\n" +
                "                      </div>\n" +
                "                      <!--<![endif]-->\n" +
                "                    </div>\n" +
                "                  </div>\n" +
                "                  <!--[if (mso)|(IE)]></td><![endif]-->\n" +
                "                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->\n" +
                "                </div>\n" +
                "              </div>\n" +
                "            </div>\n" +
                "\n" +
                "            <div\n" +
                "              class=\"u-row-container\"\n" +
                "              style=\"padding: 0px; background-color: transparent\"\n" +
                "            >\n" +
                "              <div\n" +
                "                class=\"u-row\"\n" +
                "                style=\"\n" +
                "                  margin: 0 auto;\n" +
                "                  min-width: 320px;\n" +
                "                  max-width: 600px;\n" +
                "                  overflow-wrap: break-word;\n" +
                "                  word-wrap: break-word;\n" +
                "                  word-break: break-word;\n" +
                "                  background-color: #080f30;\n" +
                "                \"\n" +
                "              >\n" +
                "                <div\n" +
                "                  style=\"\n" +
                "                    border-collapse: collapse;\n" +
                "                    display: table;\n" +
                "                    width: 100%;\n" +
                "                    height: 100%;\n" +
                "                    background-color: transparent;\n" +
                "                  \"\n" +
                "                >\n" +
                "                  <!--[if (mso)|(IE)]><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"padding: 0px;background-color: transparent;\" align=\"center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"width:600px;\"><tr style=\"background-color: #080f30;\"><![endif]-->\n" +
                "\n" +
                "                  <!--[if (mso)|(IE)]><td align=\"center\" width=\"600\" style=\"width: 600px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-radius: 0px;-webkit-border-radius: 0px; -moz-border-radius: 0px;\" valign=\"top\"><![endif]-->\n" +
                "                  <div\n" +
                "                    class=\"u-col u-col-100\"\n" +
                "                    style=\"\n" +
                "                      max-width: 320px;\n" +
                "                      min-width: 600px;\n" +
                "                      display: table-cell;\n" +
                "                      vertical-align: top;\n" +
                "                    \"\n" +
                "                  >\n" +
                "                    <div\n" +
                "                      style=\"\n" +
                "                        height: 100%;\n" +
                "                        width: 100% !important;\n" +
                "                        border-radius: 0px;\n" +
                "                        -webkit-border-radius: 0px;\n" +
                "                        -moz-border-radius: 0px;\n" +
                "                      \"\n" +
                "                    >\n" +
                "                      <!--[if (!mso)&(!IE)]><!--><div\n" +
                "                        style=\"\n" +
                "                          box-sizing: border-box;\n" +
                "                          height: 100%;\n" +
                "                          padding: 0px;\n" +
                "                          border-top: 0px solid transparent;\n" +
                "                          border-left: 0px solid transparent;\n" +
                "                          border-right: 0px solid transparent;\n" +
                "                          border-bottom: 0px solid transparent;\n" +
                "                          border-radius: 0px;\n" +
                "                          -webkit-border-radius: 0px;\n" +
                "                          -moz-border-radius: 0px;\n" +
                "                        \"\n" +
                "                      ><!--<![endif]-->\n" +
                "                        <table\n" +
                "                          style=\"font-family: arial, helvetica, sans-serif\"\n" +
                "                          role=\"presentation\"\n" +
                "                          cellpadding=\"0\"\n" +
                "                          cellspacing=\"0\"\n" +
                "                          width=\"100%\"\n" +
                "                          border=\"0\"\n" +
                "                        >\n" +
                "                          <tbody>\n" +
                "                            <tr>\n" +
                "                              <td\n" +
                "                                class=\"v-container-padding-padding\"\n" +
                "                                style=\"\n" +
                "                                  overflow-wrap: break-word;\n" +
                "                                  word-break: break-word;\n" +
                "                                  padding: 42px 10px 15px;\n" +
                "                                  font-family: arial, helvetica, sans-serif;\n" +
                "                                \"\n" +
                "                                align=\"left\"\n" +
                "                              >\n" +
                "                                <div align=\"center\">\n" +
                "                                  <div style=\"display: table; max-width: 179px\">\n" +
                "                                    <!--[if (mso)|(IE)]><table width=\"179\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td style=\"border-collapse:collapse;\" align=\"center\"><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse; mso-table-lspace: 0pt;mso-table-rspace: 0pt; width:179px;\"><tr><![endif]-->\n" +
                "\n" +
                "                                    <!--[if (mso)|(IE)]><td width=\"32\" style=\"width:32px; padding-right: 13px;\" valign=\"top\"><![endif]-->\n" +
                "                                    <table\n" +
                "                                      align=\"left\"\n" +
                "                                      border=\"0\"\n" +
                "                                      cellspacing=\"0\"\n" +
                "                                      cellpadding=\"0\"\n" +
                "                                      width=\"32\"\n" +
                "                                      height=\"32\"\n" +
                "                                      style=\"\n" +
                "                                        width: 32px !important;\n" +
                "                                        height: 32px !important;\n" +
                "                                        display: inline-block;\n" +
                "                                        border-collapse: collapse;\n" +
                "                                        table-layout: fixed;\n" +
                "                                        border-spacing: 0;\n" +
                "                                        mso-table-lspace: 0pt;\n" +
                "                                        mso-table-rspace: 0pt;\n" +
                "                                        vertical-align: top;\n" +
                "                                        margin-right: 13px;\n" +
                "                                      \"\n" +
                "                                    >\n" +
                "                                      <tbody>\n" +
                "                                        <tr style=\"vertical-align: top\">\n" +
                "                                          <td\n" +
                "                                            align=\"left\"\n" +
                "                                            valign=\"middle\"\n" +
                "                                            style=\"\n" +
                "                                              word-break: break-word;\n" +
                "                                              border-collapse: collapse !important;\n" +
                "                                              vertical-align: top;\n" +
                "                                            \"\n" +
                "                                          >\n" +
                "                                            <a\n" +
                "                                              href=\"https://linkedin.com/\"\n" +
                "                                              title=\"LinkedIn\"\n" +
                "                                              target=\"_blank\"\n" +
                "                                            >\n" +
                "                                              <img\n" +
                "                                                src=\"https://i.imgur.com/3B8cYRK.png\"\n" +
                "                                                alt=\"LinkedIn\"\n" +
                "                                                title=\"LinkedIn\"\n" +
                "                                                width=\"32\"\n" +
                "                                                style=\"\n" +
                "                                                  outline: none;\n" +
                "                                                  text-decoration: none;\n" +
                "                                                  -ms-interpolation-mode: bicubic;\n" +
                "                                                  clear: both;\n" +
                "                                                  display: block !important;\n" +
                "                                                  border: none;\n" +
                "                                                  height: auto;\n" +
                "                                                  float: none;\n" +
                "                                                  max-width: 32px !important;\n" +
                "                                                \"\n" +
                "                                              />\n" +
                "                                            </a>\n" +
                "                                          </td>\n" +
                "                                        </tr>\n" +
                "                                      </tbody>\n" +
                "                                    </table>\n" +
                "                                    <!--[if (mso)|(IE)]></td><![endif]-->\n" +
                "\n" +
                "                                    <!--[if (mso)|(IE)]><td width=\"32\" style=\"width:32px; padding-right: 13px;\" valign=\"top\"><![endif]-->\n" +
                "                                    <table\n" +
                "                                      align=\"left\"\n" +
                "                                      border=\"0\"\n" +
                "                                      cellspacing=\"0\"\n" +
                "                                      cellpadding=\"0\"\n" +
                "                                      width=\"32\"\n" +
                "                                      height=\"32\"\n" +
                "                                      style=\"\n" +
                "                                        width: 32px !important;\n" +
                "                                        height: 32px !important;\n" +
                "                                        display: inline-block;\n" +
                "                                        border-collapse: collapse;\n" +
                "                                        table-layout: fixed;\n" +
                "                                        border-spacing: 0;\n" +
                "                                        mso-table-lspace: 0pt;\n" +
                "                                        mso-table-rspace: 0pt;\n" +
                "                                        vertical-align: top;\n" +
                "                                        margin-right: 13px;\n" +
                "                                      \"\n" +
                "                                    >\n" +
                "                                      <tbody>\n" +
                "                                        <tr style=\"vertical-align: top\">\n" +
                "                                          <td\n" +
                "                                            align=\"left\"\n" +
                "                                            valign=\"middle\"\n" +
                "                                            style=\"\n" +
                "                                              word-break: break-word;\n" +
                "                                              border-collapse: collapse !important;\n" +
                "                                              vertical-align: top;\n" +
                "                                            \"\n" +
                "                                          >\n" +
                "                                            <a\n" +
                "                                              href=\"https://instagram.com/\"\n" +
                "                                              title=\"Instagram\"\n" +
                "                                              target=\"_blank\"\n" +
                "                                            >\n" +
                "                                              <img\n" +
                "                                                src=\"https://i.imgur.com/L8PypuE.png\"\n" +
                "                                                alt=\"Instagram\"\n" +
                "                                                title=\"Instagram\"\n" +
                "                                                width=\"32\"\n" +
                "                                                style=\"\n" +
                "                                                  outline: none;\n" +
                "                                                  text-decoration: none;\n" +
                "                                                  -ms-interpolation-mode: bicubic;\n" +
                "                                                  clear: both;\n" +
                "                                                  display: block !important;\n" +
                "                                                  border: none;\n" +
                "                                                  height: auto;\n" +
                "                                                  float: none;\n" +
                "                                                  max-width: 32px !important;\n" +
                "                                                \"\n" +
                "                                              />\n" +
                "                                            </a>\n" +
                "                                          </td>\n" +
                "                                        </tr>\n" +
                "                                      </tbody>\n" +
                "                                    </table>\n" +
                "                                    <!--[if (mso)|(IE)]></td><![endif]-->\n" +
                "\n" +
                "                                    <!--[if (mso)|(IE)]><td width=\"32\" style=\"width:32px; padding-right: 13px;\" valign=\"top\"><![endif]-->\n" +
                "                                    <table\n" +
                "                                      align=\"left\"\n" +
                "                                      border=\"0\"\n" +
                "                                      cellspacing=\"0\"\n" +
                "                                      cellpadding=\"0\"\n" +
                "                                      width=\"32\"\n" +
                "                                      height=\"32\"\n" +
                "                                      style=\"\n" +
                "                                        width: 32px !important;\n" +
                "                                        height: 32px !important;\n" +
                "                                        display: inline-block;\n" +
                "                                        border-collapse: collapse;\n" +
                "                                        table-layout: fixed;\n" +
                "                                        border-spacing: 0;\n" +
                "                                        mso-table-lspace: 0pt;\n" +
                "                                        mso-table-rspace: 0pt;\n" +
                "                                        vertical-align: top;\n" +
                "                                        margin-right: 13px;\n" +
                "                                      \"\n" +
                "                                    >\n" +
                "                                      <tbody>\n" +
                "                                        <tr style=\"vertical-align: top\">\n" +
                "                                          <td\n" +
                "                                            align=\"left\"\n" +
                "                                            valign=\"middle\"\n" +
                "                                            style=\"\n" +
                "                                              word-break: break-word;\n" +
                "                                              border-collapse: collapse !important;\n" +
                "                                              vertical-align: top;\n" +
                "                                            \"\n" +
                "                                          >\n" +
                "                                            <a\n" +
                "                                              href=\"https://twitter.com/\"\n" +
                "                                              title=\"Twitter\"\n" +
                "                                              target=\"_blank\"\n" +
                "                                            >\n" +
                "                                              <img\n" +
                "                                                src=\"https://i.imgur.com/LFICyhi.png\"\n" +
                "                                                alt=\"Twitter\"\n" +
                "                                                title=\"Twitter\"\n" +
                "                                                width=\"32\"\n" +
                "                                                style=\"\n" +
                "                                                  outline: none;\n" +
                "                                                  text-decoration: none;\n" +
                "                                                  -ms-interpolation-mode: bicubic;\n" +
                "                                                  clear: both;\n" +
                "                                                  display: block !important;\n" +
                "                                                  border: none;\n" +
                "                                                  height: auto;\n" +
                "                                                  float: none;\n" +
                "                                                  max-width: 32px !important;\n" +
                "                                                \"\n" +
                "                                              />\n" +
                "                                            </a>\n" +
                "                                          </td>\n" +
                "                                        </tr>\n" +
                "                                      </tbody>\n" +
                "                                    </table>\n" +
                "                                    <!--[if (mso)|(IE)]></td><![endif]-->\n" +
                "\n" +
                "                                    <!--[if (mso)|(IE)]><td width=\"32\" style=\"width:32px; padding-right: 0px;\" valign=\"top\"><![endif]-->\n" +
                "                                    <table\n" +
                "                                      align=\"left\"\n" +
                "                                      border=\"0\"\n" +
                "                                      cellspacing=\"0\"\n" +
                "                                      cellpadding=\"0\"\n" +
                "                                      width=\"32\"\n" +
                "                                      height=\"32\"\n" +
                "                                      style=\"\n" +
                "                                        width: 32px !important;\n" +
                "                                        height: 32px !important;\n" +
                "                                        display: inline-block;\n" +
                "                                        border-collapse: collapse;\n" +
                "                                        table-layout: fixed;\n" +
                "                                        border-spacing: 0;\n" +
                "                                        mso-table-lspace: 0pt;\n" +
                "                                        mso-table-rspace: 0pt;\n" +
                "                                        vertical-align: top;\n" +
                "                                        margin-right: 0px;\n" +
                "                                      \"\n" +
                "                                    >\n" +
                "                                      <tbody>\n" +
                "                                        <tr style=\"vertical-align: top\">\n" +
                "                                          <td\n" +
                "                                            align=\"left\"\n" +
                "                                            valign=\"middle\"\n" +
                "                                            style=\"\n" +
                "                                              word-break: break-word;\n" +
                "                                              border-collapse: collapse !important;\n" +
                "                                              vertical-align: top;\n" +
                "                                            \"\n" +
                "                                          >\n" +
                "                                            <a\n" +
                "                                              href=\"https://pinterest.com/\"\n" +
                "                                              title=\"Pinterest\"\n" +
                "                                              target=\"_blank\"\n" +
                "                                            >\n" +
                "                                              <img\n" +
                "                                                src=\"https://i.imgur.com/3xwDosC.png\"\n" +
                "                                                alt=\"Pinterest\"\n" +
                "                                                title=\"Pinterest\"\n" +
                "                                                width=\"32\"\n" +
                "                                                style=\"\n" +
                "                                                  outline: none;\n" +
                "                                                  text-decoration: none;\n" +
                "                                                  -ms-interpolation-mode: bicubic;\n" +
                "                                                  clear: both;\n" +
                "                                                  display: block !important;\n" +
                "                                                  border: none;\n" +
                "                                                  height: auto;\n" +
                "                                                  float: none;\n" +
                "                                                  max-width: 32px !important;\n" +
                "                                                \"\n" +
                "                                              />\n" +
                "                                            </a>\n" +
                "                                          </td>\n" +
                "                                        </tr>\n" +
                "                                      </tbody>\n" +
                "                                    </table>\n" +
                "                                    <!--[if (mso)|(IE)]></td><![endif]-->\n" +
                "\n" +
                "                                    <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->\n" +
                "                                  </div>\n" +
                "                                </div>\n" +
                "                              </td>\n" +
                "                            </tr>\n" +
                "                          </tbody>\n" +
                "                        </table>\n" +
                "\n" +
                "                        <table\n" +
                "                          style=\"font-family: arial, helvetica, sans-serif\"\n" +
                "                          role=\"presentation\"\n" +
                "                          cellpadding=\"0\"\n" +
                "                          cellspacing=\"0\"\n" +
                "                          width=\"100%\"\n" +
                "                          border=\"0\"\n" +
                "                        >\n" +
                "                          <tbody>\n" +
                "                            <tr>\n" +
                "                              <td\n" +
                "                                class=\"v-container-padding-padding\"\n" +
                "                                style=\"\n" +
                "                                  overflow-wrap: break-word;\n" +
                "                                  word-break: break-word;\n" +
                "                                  padding: 10px 10px 35px;\n" +
                "                                  font-family: arial, helvetica, sans-serif;\n" +
                "                                \"\n" +
                "                                align=\"left\"\n" +
                "                              >\n" +
                "                                <div\n" +
                "                                  class=\"v-text-align v-font-size\"\n" +
                "                                  style=\"\n" +
                "                                    color: #ffffff;\n" +
                "                                    line-height: 210%;\n" +
                "                                    text-align: center;\n" +
                "                                    word-wrap: break-word;\n" +
                "                                  \"\n" +
                "                                >\n" +
                "                                  <p style=\"font-size: 14px; line-height: 210%\">\n" +
                "                                    <span\n" +
                "                                      style=\"\n" +
                "                                        font-family: Lato, sans-serif;\n" +
                "                                        font-size: 14px;\n" +
                "                                        line-height: 29.4px;\n" +
                "                                      \"\n" +
                "                                      >You're receiving this email because you\n" +
                "                                      asked us about registration.</span\n" +
                "                                    >\n" +
                "                                  </p>\n" +
                "                                  <p style=\"font-size: 14px; line-height: 210%\">\n" +
                "                                    <span\n" +
                "                                      style=\"\n" +
                "                                        font-family: Lato, sans-serif;\n" +
                "                                        font-size: 14px;\n" +
                "                                        line-height: 29.4px;\n" +
                "                                      \"\n" +
                "                                      >©2023 CAT-ER | FERIT Osijek, Ul. Kneza\n" +
                "                                      Trpimira 2b</span\n" +
                "                                    >\n" +
                "                                  </p>\n" +
                "                                </div>\n" +
                "                              </td>\n" +
                "                            </tr>\n" +
                "                          </tbody>\n" +
                "                        </table>\n" +
                "\n" +
                "                        <!--[if (!mso)&(!IE)]><!-->\n" +
                "                      </div>\n" +
                "                      <!--<![endif]-->\n" +
                "                    </div>\n" +
                "                  </div>\n" +
                "                  <!--[if (mso)|(IE)]></td><![endif]-->\n" +
                "                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->\n" +
                "                </div>\n" +
                "              </div>\n" +
                "            </div>\n" +
                "\n" +
                "            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->\n" +
                "          </td>\n" +
                "        </tr>\n" +
                "      </tbody>\n" +
                "    </table>\n" +
                "    <!--[if mso]></div><![endif]-->\n" +
                "    <!--[if IE]></div><![endif]-->\n" +
                "  </body>\n" +
                "</html>\n";
    }
}
