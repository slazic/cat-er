package com.slazic.CATER.utility;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@NoArgsConstructor
public class StringGenerator {

    private static final int TEST_EMAIL_LENGTH = 32;
    private static final int LEFT_LIMIT = 97;
    private static final int RIGHT_LIMIT = 122;

    public static @NotNull String generateRandomString(final @NotNull int length) {

        final SecureRandom random = new SecureRandom();
        final StringBuilder builder = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            int randomInt = LEFT_LIMIT + (int) (random.nextFloat() * (RIGHT_LIMIT - LEFT_LIMIT + 1));
            builder.append((char) randomInt);
        }

        return builder.toString();
    }

    public static @NotNull String generateRandomEmail() {
        final List<String> domains = Arrays.asList("etfos.hr", "gmail.com", "yahoo.com", "hotmail.com", "outlook.com");
        final Random random = new Random();
        final String username = generateRandomString(TEST_EMAIL_LENGTH);
        final String domain = domains.get(random.nextInt(domains.size()));
        return username + "@" + domain;
    }
}
