package com.slazic.CATER.utility;


import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.regex.Pattern;

@NoArgsConstructor
public class EmailValidator {

    private static final Pattern EMAIL_PATTERN = Pattern.compile(
            "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$"
    );

    public static boolean test(final @NotNull String email) {
        return EMAIL_PATTERN.matcher(email).matches();
    }
}
