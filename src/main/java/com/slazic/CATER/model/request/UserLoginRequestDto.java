package com.slazic.CATER.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class UserLoginRequestDto {

    @NotBlank(message = "Email null, empty or blank.")
    @Size(max = 255, message = "Email can be long only 255 characters")
    private String email;

    @NotBlank(message = "Password null, empty or blank.")
    @Size(max = 255, message = "Password can be long only 255 characters")
    private String password;
}
