package com.slazic.CATER.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class UserChangePasswordDto {
    @NotBlank(message = "Old password null, empty or blank.")
    @Size(max = 255, message = "Password can be long only 255 characters")
    private String oldPassword;

    @NotBlank(message = "New password null, empty or blank.")
    @Size(max = 255, message = "Password can be long only 255 characters")
    private String newPassword;
}
