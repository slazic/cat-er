package com.slazic.CATER.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class UserRegisterRequestDto {

    @NotBlank(message = "First name null, empty or blank.")
    @Size(max = 255, message = "First name can be long only 255 characters")
    private String firstName;

    @NotBlank(message = "Last name null, empty or blank.")
    @Size(max = 255, message = "Last name can be long only 255 characters")
    private String lastName;

    @NotBlank(message = "Email null, empty or blank.")
    @Size(max = 255, message = "Email can be long only 255 characters")
    private String email;

    @NotBlank(message = "Password null, empty or blank.")
    @Size(max = 255, message = "Password can be long only 255 characters")
    private String password;
}
