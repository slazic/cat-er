package com.slazic.CATER.model.mapper;

import java.util.List;

public interface EntityDtoMapper<T, U> {

    U map(final T value);

    List<U> mapToList(final List<T> values);
}
