package com.slazic.CATER.model.mapper.implementation;

import com.slazic.CATER.model.User;
import com.slazic.CATER.model.enums.UserRole;
import com.slazic.CATER.model.mapper.EntityDtoMapper;
import com.slazic.CATER.model.request.UserRegisterRequestDto;
import com.slazic.CATER.utility.Sha256Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserRegisterRequestDtoToUserMapper implements EntityDtoMapper<UserRegisterRequestDto, User> {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserRegisterRequestDtoToUserMapper(final @NotNull PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public @NotNull User map(@NotNull final UserRegisterRequestDto userRegisterRequestDto) {
        final User user = new User();

        user.setFirstName(userRegisterRequestDto.getFirstName());
        user.setLastName(userRegisterRequestDto.getLastName());
        user.setEmail(userRegisterRequestDto.getEmail());
        user.setPassword(passwordEncoder.encode(userRegisterRequestDto.getPassword()));
        user.setUserRole(UserRole.USER);
        user.setCreatedAt(LocalDateTime.now());
        user.setVerified(false);
        user.setLocked(false);
        user.setHashedIdentifier(Sha256Utility.getHash(userRegisterRequestDto.getEmail()));

        return user;
    }

    @Override
    public @NotNull List<User> mapToList(@NotNull final List<UserRegisterRequestDto> userRegisterRequestDtos) {
        return userRegisterRequestDtos
                .stream()
                .map(this::map)
                .collect(Collectors.toList());
    }
}
