package com.slazic.CATER.model.response.external.binance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class BinanceTickerPrice {
    private String symbol;
    private String price;
}
