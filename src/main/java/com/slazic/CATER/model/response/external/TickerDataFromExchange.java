package com.slazic.CATER.model.response.external;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class TickerDataFromExchange {
    private String exchangeName;
    private String price;
}
