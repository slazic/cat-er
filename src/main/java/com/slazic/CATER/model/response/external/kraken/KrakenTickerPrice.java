package com.slazic.CATER.model.response.external.kraken;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class KrakenTickerPrice {
    private List<Object> error;
    private Map<String, KrakenTickerInfo> result;
}
