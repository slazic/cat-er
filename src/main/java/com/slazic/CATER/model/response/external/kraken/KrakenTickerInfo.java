package com.slazic.CATER.model.response.external.kraken;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class KrakenTickerInfo {
    private List<String> a;
    private List<String> b;
    private List<String> c;
    private List<String> v;
    private List<String> p;
    private List<Integer> t;
    private List<String> l;
    private List<String> h;
}
