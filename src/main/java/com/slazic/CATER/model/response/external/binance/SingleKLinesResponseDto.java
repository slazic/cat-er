package com.slazic.CATER.model.response.external.binance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class SingleKLinesResponseDto {
    private Long x;
    private List<BigDecimal> y;
}
