package com.slazic.CATER.model.enums;

public enum UserRole {
    USER,
    ADMIN;
}
