package com.slazic.CATER.service;

import com.slazic.CATER.configuration.email.EmailProperties;
import com.slazic.CATER.configuration.security.JwtService;
import com.slazic.CATER.exception.EmailAlreadyInUseException;
import com.slazic.CATER.model.User;
import com.slazic.CATER.model.request.UserChangePasswordDto;
import com.slazic.CATER.repository.UserRepository;
import com.slazic.CATER.utility.EmailValidator;
import jakarta.mail.MessagingException;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    private final JwtService jwtService;

    private final EmailService emailService;

    private final EmailProperties emailProperties;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(
            final @NotNull UserRepository userRepository,
            final @NotNull JwtService jwtService,
            final @NotNull EmailService emailService,
            final @NotNull EmailProperties emailProperties,
            final @NotNull PasswordEncoder passwordEncoder
    ) {
        this.userRepository = userRepository;
        this.jwtService = jwtService;
        this.emailService = emailService;
        this.emailProperties = emailProperties;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(final @NotNull String username) throws UsernameNotFoundException {
        return userRepository.findByEmail(username).orElseThrow(
                () -> new UsernameNotFoundException("User with email " + username + " not found.")
        );
    }

    //IMPORTANT: custom method loadUserByEmail returns full UserObject instead of just UserDetails if needed
    public User loadUserByEmail(final @NotNull String email) throws UsernameNotFoundException {
        return userRepository.findByEmail(email).orElseThrow(
                () -> new UsernameNotFoundException("User with email " + email + " not found.")
        );
    }

    public @NotNull String getEmailAlreadyInUseErrorMessage() {
        return "This e-mail already used.";
    }

    public @NotNull String getInvalidCredentialsErrorMessage() {
        return "Bad credentials.";
    }

    public @NotNull String getEmailNotVerifiedErrorMessage() {
        return "Your account is not verified yet.";
    }

    public @NotNull String getEmailNotRegisteredMessage() {
        return "This e-mail is not registered.";
    }

    public @NotNull boolean canVerifyEmail(final @NotNull LocalDateTime createdAt) {
        final ZonedDateTime dateTime = ZonedDateTime.of(createdAt, ZoneId.systemDefault());
        return System.currentTimeMillis() < dateTime.toInstant().toEpochMilli() + emailProperties.getDuration();
    }

    public @NotNull boolean isBlocked(final @NotNull User user) {
        return user.getLocked();
    }

    @Transactional
    public void register(final @NotNull User user) throws EmailAlreadyInUseException, MessagingException {
        UserDetails savedUser = null;

        try {
            savedUser = loadUserByUsername(user.getEmail());
        } catch (UsernameNotFoundException e) {
            /*ignore*/
        }

        if (savedUser != null)
            throw new EmailAlreadyInUseException(getEmailAlreadyInUseErrorMessage());

        if (!EmailValidator.test(user.getEmail()))
            throw new IllegalStateException("Email: " + user.getEmail() + " is not valid.");

        log.info("SEND TO: "+ user.getEmail() + " " + user.getHashedIdentifier());
        emailService.sendVerificationEmail(user.getEmail(), user.getHashedIdentifier());

        userRepository.save(user);
    }

    public @NotNull String authenticate(final @NotNull UserDetails userDetails) {
        return jwtService.generateToken(userDetails);
    }

    public @NotNull void verify(final @NotNull String hashedIdentifier) throws IllegalAccessException {

        final Optional<User> user = userRepository.findByHashedIdentifier(hashedIdentifier);

        if (user.isEmpty() || user.get().isEnabled() || !canVerifyEmail(user.get().getCreatedAt())) {
            log.warn("User with hash '{}' tried to verify e-mail", hashedIdentifier);
            throw new IllegalAccessException("User with hash '" + hashedIdentifier + "' tried to verify e-mail");
        }

        user.get().setVerified(true);

        userRepository.save(user.get());
    }

    public @NotNull List<User> fetchAllUsers() {
        return userRepository.findAll();
    }

    public @NotNull void changeUserAccess(final @NotNull Long id, final @NotNull Boolean access) {
        final Optional<User> user = userRepository.findById(id);

        if (user.isEmpty()) {
            throw new ObjectNotFoundException(id, "User");
        }

        user.get().setLocked(access);

        userRepository.save(user.get());
    }

    public @NotNull void changeUserPassword (final @NotNull UserChangePasswordDto dto, final @NotNull String username) {
        final Optional<User> user = userRepository.findByEmail(username);

        if (user.isEmpty()) {
            throw new ObjectNotFoundException(user, "User");
        }

        if(!passwordEncoder.matches(dto.getOldPassword(), user.get().getPassword())) {
            throw new IllegalArgumentException("Old password is incorrect.");
        }

        user.get().setPassword(passwordEncoder.encode(dto.getNewPassword()));

        userRepository.save(user.get());
    }
}
