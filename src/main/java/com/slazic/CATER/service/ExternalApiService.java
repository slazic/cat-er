package com.slazic.CATER.service;

import com.slazic.CATER.model.response.external.TickerDataFromExchange;
import com.slazic.CATER.model.response.external.binance.BinanceTickerPrice;
import com.slazic.CATER.model.response.external.binance.SingleKLinesResponseDto;
import com.slazic.CATER.model.response.external.kraken.KrakenTickerPrice;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.*;

@Service
public class ExternalApiService {

    private final RestTemplate restTemplate;

    public ExternalApiService(
            final RestTemplateBuilder restTemplateBuilder
    ) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public List<List<List<Object>>> fetch5MinIntervalCandlestickDataFromBinance(
            final List<String> symbols
    ) {
        final long localUnixTimestamp = System.currentTimeMillis();
        final long startTime = localUnixTimestamp - (24 * 60 * 60 * 1000);
        final List<List<List<Object>>> finalExternalResponseData = new ArrayList<>();

        for(final String symbol : symbols) {
            final String url = "https://api.binance.com/api/v3/uiKlines?symbol="+ symbol +"&interval=5m&startTime=" + startTime;
            final List<List<Object>> externalResponseData = restTemplate.getForObject(url, List.class);
            finalExternalResponseData.add(externalResponseData);
        }

        final List<List<List<Object>>> finalLocalResponseData = new ArrayList<>();

        for (List<List<Object>> objects : finalExternalResponseData) {
            List<List<Object>> localResponseData = new ArrayList<>();
            for (List<Object> object : objects) {
                Long closeTime = Long.parseLong(object.get(6).toString());
                BigDecimal closePrice = new BigDecimal(object.get(4).toString());

                List<Object> CandlestickData = new ArrayList<>();
                CandlestickData.add(closeTime);
                CandlestickData.add(closePrice);

                localResponseData.add(CandlestickData);
            }
            finalLocalResponseData.add(localResponseData);
        }

        return finalLocalResponseData;
    }

    public Object fetchSingleCandlestickDataFromSelectedMarket(final String symbol, final String timeframe, final String exchangeName) throws NoSuchFieldException, IllegalAccessException {
        final long localUnixTimestamp = System.currentTimeMillis();
        final long last8hStartTime = localUnixTimestamp - (8 * 60 * 60 * 1000);
        final long last1MothStartTimeMinus11days = localUnixTimestamp - (31L * 24 * 60 * 60 * 1000 - 11L * 24 * 60 * 60 * 1000);
        final long last2YearsStartTimeMinus8Months = localUnixTimestamp - (2L * 365 * 24 * 60 * 60 * 1000 - 8L * 30 * 24 * 60 * 60 * 1000);
        final long last9YearsStartTime = localUnixTimestamp - (9L * 365 * 24 * 60 * 60 * 1000);
        final long last10YearsStartTime = localUnixTimestamp - (10L * 365 * 24 * 60 * 60 * 1000);

        List<List<Object>> externalBinanceResponseData = new ArrayList<>();

        List<SingleKLinesResponseDto> finalLocalResponseData = new ArrayList<>();

        final String oneMinUrlB = "https://api.binance.com/api/v3/uiKlines?symbol="+ symbol +"&interval=1m&startTime=" + last8hStartTime;
        final String oneHourUrlB = "https://api.binance.com/api/v3/uiKlines?symbol="+ symbol +"&interval=1h&startTime=" + last1MothStartTimeMinus11days;
        final String oneDayUrlB = "https://api.binance.com/api/v3/uiKlines?symbol="+ symbol +"&interval=1d&startTime=" + last2YearsStartTimeMinus8Months;
        final String oneWeekUrlB = "https://api.binance.com/api/v3/uiKlines?symbol="+ symbol +"&interval=1w&startTime=" + last9YearsStartTime;
        final String oneMonthUrlB = "https://api.binance.com/api/v3/uiKlines?symbol="+ symbol +"&interval=1M&startTime=" + last10YearsStartTime;

        final String oneMinUrlK = "https://api.kraken.com/0/public/OHLC?pair="+ symbol +"&interval=1&since=" + last8hStartTime;
        final String oneHourUrlK = "https://api.kraken.com/0/public/OHLC?pair="+ symbol +"&interval=60&since=" + last1MothStartTimeMinus11days;
        final String oneDayUrlK = "https://api.kraken.com/0/public/OHLC?pair="+ symbol +"&interval=1440&since=" + last2YearsStartTimeMinus8Months;
        final String oneWeekUrlK = "https://api.kraken.com/0/public/OHLC?pair="+ symbol +"&interval=10080&since=" + last9YearsStartTime;
        final String oneMonthUrlK = "https://api.kraken.com/0/public/OHLC?pair="+ symbol +"&interval=40320&since=" + last10YearsStartTime;

        if (Objects.equals(exchangeName, "Binance")) {
            externalBinanceResponseData = switch (timeframe) {
                case "1m" -> restTemplate.getForObject(oneMinUrlB, List.class);
                case "1h" -> restTemplate.getForObject(oneHourUrlB, List.class);
                case "D" -> restTemplate.getForObject(oneDayUrlB, List.class);
                case "W" -> restTemplate.getForObject(oneWeekUrlB, List.class);
                case "M" -> restTemplate.getForObject(oneMonthUrlB, List.class);
                default -> externalBinanceResponseData;
            };

            for(List<Object> object: externalBinanceResponseData) {
                Long openTime = Long.parseLong(object.get(0).toString());
                final List<BigDecimal> kLineData = new ArrayList<>();

                BigDecimal openPrice = new BigDecimal(object.get(1).toString());
                BigDecimal highPrice = new BigDecimal(object.get(2).toString());
                BigDecimal lowPrice = new BigDecimal(object.get(3).toString());
                BigDecimal closePrice = new BigDecimal(object.get(4).toString());

                kLineData.add(openPrice);
                kLineData.add(highPrice);
                kLineData.add(lowPrice);
                kLineData.add(closePrice);

                SingleKLinesResponseDto singleKLinesResponseDto = new SingleKLinesResponseDto();
                singleKLinesResponseDto.setX(openTime);
                singleKLinesResponseDto.setY(kLineData);

                finalLocalResponseData.add(singleKLinesResponseDto);
            }
        } else if (Objects.equals(exchangeName, "Kraken")) {

            LinkedHashMap<String, LinkedHashMap<String, Object>> objectR = new LinkedHashMap<>();

            objectR = switch (timeframe) {
                case "1m" -> restTemplate.getForObject(oneMinUrlK, LinkedHashMap.class);
                case "1h" -> restTemplate.getForObject(oneHourUrlK, LinkedHashMap.class);
                case "D" -> restTemplate.getForObject(oneDayUrlK, LinkedHashMap.class);
                case "W" -> restTemplate.getForObject(oneWeekUrlK, LinkedHashMap.class);
                case "M" -> restTemplate.getForObject(oneMonthUrlK, LinkedHashMap.class);
                default -> objectR;
            };

            LinkedHashMap<String, Object> firstResult = objectR.get("result");
            String keyName = firstResult.keySet().stream().findFirst().get();
            List<List<Object>> results = (List<List<Object>>) firstResult.get(keyName);

            for (List<Object> object: results) {
                Long openTime = Long.parseLong(object.get(0).toString());
                final List<BigDecimal> kLineData = new ArrayList<>();

                BigDecimal openPrice = new BigDecimal(object.get(1).toString());
                BigDecimal highPrice = new BigDecimal(object.get(2).toString());
                BigDecimal lowPrice = new BigDecimal(object.get(3).toString());
                BigDecimal closePrice = new BigDecimal(object.get(4).toString());

                kLineData.add(openPrice);
                kLineData.add(highPrice);
                kLineData.add(lowPrice);
                kLineData.add(closePrice);

                SingleKLinesResponseDto singleKLinesResponseDto = new SingleKLinesResponseDto();
                singleKLinesResponseDto.setX(openTime);
                singleKLinesResponseDto.setY(kLineData);

                finalLocalResponseData.add(singleKLinesResponseDto);
            }
        }

        return finalLocalResponseData;
    }

    public List<Object> fetchTickerDataFromAllExchanges(final String symbol) {
        final String urlBinance = "https://api.binance.com/api/v3/ticker/price?symbol=" + symbol;
        final String urlKraken = "https://api.kraken.com/0/public/Ticker?pair=" + symbol;

        List<Object> finalLocalResponseData = new ArrayList<>();

        final BinanceTickerPrice binanceResponseData = restTemplate.getForObject(urlBinance, BinanceTickerPrice.class);
        final KrakenTickerPrice krakenResponseData = restTemplate.getForObject(urlKraken, KrakenTickerPrice.class);

        final String krakenResultKeyName = krakenResponseData.getResult().keySet().stream().findFirst().get();
        final String krakenResultPrice = krakenResponseData.getResult().get(krakenResultKeyName).getC().get(0);

        TickerDataFromExchange binanceResponse = new TickerDataFromExchange();
        binanceResponse.setExchangeName("Binance");
        binanceResponse.setPrice(binanceResponseData.getPrice());

        TickerDataFromExchange krakenResponse = new TickerDataFromExchange();
        krakenResponse.setExchangeName("Kraken");
        krakenResponse.setPrice(krakenResultPrice);

        finalLocalResponseData.add(binanceResponse);
        finalLocalResponseData.add(krakenResponse);

        return finalLocalResponseData;
    }
}
