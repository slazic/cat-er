package com.slazic.CATER.service;

import com.slazic.CATER.configuration.email.EmailProperties;
import com.slazic.CATER.model.User;
import com.slazic.CATER.repository.UserRepository;
import com.slazic.CATER.utility.VerificationEmail;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Service
@Slf4j
@Async
public class EmailService {

    private final JavaMailSender mailSender;

    private final EmailProperties emailProperties;

    private final UserRepository userRepository;

    @Autowired
    public EmailService(
            final @NotNull JavaMailSender mailSender,
            final @NotNull EmailProperties emailProperties,
            UserRepository userRepository) {
        this.mailSender = mailSender;
        this.emailProperties = emailProperties;
        this.userRepository = userRepository;
    }

    public void sendVerificationEmail(
            @NotNull final String recipient,
            @NotNull final String hashedIdentifier
    ) throws MessagingException {
        try {
            final MimeMessage message = mailSender.createMimeMessage();
            final MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");

            final String email = VerificationEmail.buildEmail(emailProperties.getRedirectUrl(), hashedIdentifier);
            /*final String email = "<p>Poštovani,</p><p><br></p>"
                    + "<p>molimo Vas da potvrdite adresu e-pošte klikom na link</p>"
                    + "<p>koji će Vas preusmjeriti na naslovnicu kao prijavljenog korisnika:</p>"
                    + String.format("<a href=\"%s?hash=%s\">Link za potvrdu e-pošte</a>", emailProperties.getRedirectUrl(), hashedIdentifier)
                    + "<p><br></p><p>Valjanost linka istječe nakon 24 sata.</p>";*/

            //log.info(email);
            helper.setText(email, true);
            helper.setTo(recipient);
            helper.setSubject("Verify your Email address");
            helper.setFrom(emailProperties.getSender());

            mailSender.send(message);

        } catch (final MessagingException exception) {
            log.error("Failed to send email.", exception);
            throw new MessagingException("Failed to send email.");
        }
    }

    public void resendVerificationEmail(
            @NotNull final User user
    ) throws MessagingException {
        sendVerificationEmail(user.getEmail(), user.getHashedIdentifier());
        user.setCreatedAt(LocalDateTime.now());
        userRepository.save(user);
    }
}
