package com.slazic.CATER;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatErApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatErApplication.class, args);
	}

}
