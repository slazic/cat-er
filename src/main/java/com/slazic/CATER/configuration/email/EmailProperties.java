package com.slazic.CATER.configuration.email;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("cater.mail")
public class EmailProperties {

    @Value("${cater.mail.duration}")
    private Long duration;

    @Value("${cater.mail.sender}")
    private String sender;

    @Value("${cater.mail.redirect.url}")
    private String redirectUrl;
}
