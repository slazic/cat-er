package com.slazic.CATER.configuration.security;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("cater.jwt")
public class JwtProperties {

    @Value("${cater.jwt.key}")
    private String key;

    @Value("${cater.jwt.duration}")
    private Long duration;

    @Value("${cater.jwt.issuer}")
    private String issuer;
}
