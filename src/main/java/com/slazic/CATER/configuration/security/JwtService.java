package com.slazic.CATER.configuration.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class JwtService {

    private final JwtProperties jwtProperties;

    @Autowired
    public JwtService(JwtProperties jwtProperties) {
        this.jwtProperties = jwtProperties;
    }

    public @NotNull String extractUsername(final @NotNull String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public @NotNull Date extractExpiration(final @NotNull String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public boolean isTokenValid(
            final @NotNull String token,
            final @NotNull UserDetails userDetails
    ) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }

    private boolean isTokenExpired(final @NotNull String token) {
        return extractExpiration(token).before(new Date());
    }

    private @NotNull <T> T extractClaim(
            final @NotNull String token,
            final @NotNull Function<Claims, T> claimsResolver
    ) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private @NotNull Claims extractAllClaims(final @NotNull String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSigningKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private @NotNull Key getSigningKey() {
        return Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtProperties.getKey()));
    }

    public @NotNull String generateToken(final @NotNull UserDetails userDetails) {
        final String userRole = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining());

        final Map<String, Object> extraClaims = new HashMap<String,Object>();

        extraClaims.put("role", userRole);

        return generateToken(extraClaims, userDetails);
    }

    public @NotNull String generateToken(
            final @NotNull Map<String, Object> extraClaims,
            final @NotNull UserDetails userDetails
    ) {
        return Jwts
                .builder()
                .addClaims(extraClaims)
                .setSubject(userDetails.getUsername())
                .setIssuer(jwtProperties.getIssuer())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + jwtProperties.getDuration()))
                .signWith(getSigningKey(), SignatureAlgorithm.HS256)
                .compact();
    }

}
